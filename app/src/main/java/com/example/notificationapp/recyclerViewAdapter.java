package com.example.notificationapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.notificationapp.model.recyclerViewModel;

import java.util.List;

public class recyclerViewAdapter extends RecyclerView.Adapter<recyclerViewAdapter.ViewHolder> {

private List<recyclerViewModel> recyclerList;

public class ViewHolder extends RecyclerView.ViewHolder
{
    public TextView noteText,noteTime;

    public ViewHolder(View view){
        super(view);
        noteText = view.findViewById(R.id.noteText);
        noteTime = view.findViewById(R.id.noteTime);
        }
    }

public recyclerViewAdapter(List<recyclerViewModel> recyclerList){
this.recyclerList = recyclerList;
}

@Override
public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.recyclerview_row,parent,false);

return new ViewHolder(itemView);
}

@Override
public void onBindViewHolder(ViewHolder holder, int position){
recyclerViewModel model = recyclerList.get(position);
holder.noteText.setText(model.getNoteText());
holder.noteTime.setText(model.getHour());
}

@Override
    public int getItemCount(){
    return recyclerList.size();
}

}
