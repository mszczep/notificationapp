package com.example.notificationapp;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;


public class AlarmNotificationService extends IntentService {

    private NotificationManager alarmNotificationManager;
    public int NOTIFICATION_ID=1;
    public AlarmNotificationService(){
        super("AlarmNotificationService");
    }

@Override
    public void onHandleIntent(Intent intent){
        sendNotification("I'm the alarm you set up");
}

    private void sendNotification(String msg){
        alarmNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(this,0,new Intent(this,MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        String CHANNEL_ID="myChannelID";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,"myChannel",NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription("My channel");
            channel.setShowBadge(false);
            alarmNotificationManager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder alamNotificationBuilder = new NotificationCompat.Builder(this,CHANNEL_ID)
                .setContentTitle("Notifications work!")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg).setAutoCancel(true);
        alamNotificationBuilder.setContentIntent(contentIntent);
        alarmNotificationManager.notify(NOTIFICATION_ID, alamNotificationBuilder.build());

    }
}
