package com.example.notificationapp.model;


public class recyclerViewModel {

    private long id;
    private String mNoteText;
    private String mHour;

    public recyclerViewModel(long id, String noteText, String hour) {
        this.id = id;
        this.mNoteText = noteText;
        this.mHour = hour;
    }

    public long getId() { return id; }
    public void setId(long id) { this.id=id; }

    public String getNoteText()
    {
        return mNoteText;
    }
    public void setNoteText(String noteText)
    {
    this.mNoteText=noteText;
    }

    public String getHour()
    {
        return mHour;
    }
    public void setHour(String hour)
    {
        this.mHour=hour;
    }

}
