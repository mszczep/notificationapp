package com.example.notificationapp;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;
import com.example.notificationapp.db.DBadapter;
import com.example.notificationapp.model.recyclerViewModel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<recyclerViewModel> recyclerList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Menu globalMenuItem;
    private recyclerViewAdapter adapter;
    private EditText editText, editTime;
    private DBadapter dbadapter;
    private Cursor cursor;
    TimePickerDialog timePickerDialog;
    private PendingIntent pendingIntent;
    Calendar calendar;
    int currentHour, currentMin;
    public int ALARM_REQUEST_CODE = 1;
    private static Bundle mBundleRecyclerViewState;
    private final String KEY_RECYCLER_STATE = "recycler_state";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = findViewById(R.id.RecyclerView_activityMain);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        editText = findViewById(R.id.editText);
        editTime = findViewById(R.id.editTime);
        dbadapter = new DBadapter(getApplicationContext());
        dbadapter.open();
        getContent();
        adapter = new recyclerViewAdapter(recyclerList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onDestroy()
    {
        if(dbadapter!=null)
        {
            dbadapter.close();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        mBundleRecyclerViewState = new Bundle();
        Parcelable listState = recyclerView.getLayoutManager().onSaveInstanceState();
        mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE,listState);
    }
    @Override
    protected void onResume()
    {
        super.onResume();
        if(mBundleRecyclerViewState!=null)
        {
            Parcelable listState = mBundleRecyclerViewState.getParcelable(KEY_RECYCLER_STATE);
            recyclerView.getLayoutManager().onRestoreInstanceState(listState);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        globalMenuItem=menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.add_position)
        {
            editText.setVisibility(View.VISIBLE);
            editTime.setVisibility(View.VISIBLE);
            item.setVisible(false);
            globalMenuItem.findItem(R.id.menu_accept).setVisible(true);
            globalMenuItem.findItem(R.id.menu_cancel).setVisible(true);
            editText.setText("");
            editTime.setText("");
        }

        if (id == R.id.menu_accept)
        {
            String alarmTriggerTime = editTime.getText().toString();
            if(alarmTriggerTime.length()<=0) {
                Toast.makeText(this, "Please input time!", Toast.LENGTH_SHORT).show();
            }
            else {
                String[] msg = alarmTriggerTime.split(":");
                String mHour = msg[0];
                String mMin = msg[1];
                int hour = Integer.parseInt(mHour);
                int min = Integer.parseInt(mMin);
                editText.setVisibility(View.GONE);
                editTime.setVisibility(View.GONE);
                item.setVisible(false);
                globalMenuItem.findItem(R.id.menu_cancel).setVisible(false);
                globalMenuItem.findItem(R.id.add_position).setVisible(true);
                addToList();
                triggerAlarmManager(hour, min);
            }
        }
        if (id == R.id.menu_cancel)
        {
            editText.setVisibility(View.GONE);
            editTime.setVisibility(View.GONE);
            item.setVisible(false);
            globalMenuItem.findItem(R.id.menu_accept).setVisible(false);
            globalMenuItem.findItem(R.id.add_position).setVisible(true);
        }

        if(id == R.id.clear_all)
        {
            dbadapter.deleteAllContent();
            updateList();
            recyclerList.clear();
            adapter.notifyDataSetChanged();
        }
        return super.onOptionsItemSelected(item);
    }

    private void addToList(){
        String text = editText.getText().toString();
        String time = editTime.getText().toString();

        dbadapter.insertContent(text,time);
        cursor.requery();
        recyclerList.clear();
        updateList();
        adapter.notifyDataSetChanged();
}

    private void updateList() {
        if(cursor!=null && cursor.moveToFirst())
        {
            do {
                long id = cursor.getLong(DBadapter.ID_COLUMN);
                String noteText = cursor.getString(DBadapter.DESCRIPTION_COLUMN);
                String hour = cursor.getString(DBadapter.TIME_COLUMN);
                recyclerList.add(new recyclerViewModel(id,noteText,hour));
            }
            while(cursor.moveToNext());
        }
    }

    private void getContent()
    {
        recyclerList = new ArrayList<>();
        cursor = getAllEntriesFromDB();
        updateList();
    }

    private Cursor getAllEntriesFromDB() {
        cursor = dbadapter.getAllContent();
        if(cursor!=null)
        {
            startManagingCursor(cursor);
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void triggerAlarmManager(int hour, int min){
    Intent alarmIntent = new Intent(MainActivity.this, AlarmReceiver.class);
    pendingIntent = PendingIntent.getBroadcast(MainActivity.this, ALARM_REQUEST_CODE, alarmIntent,0);
    ALARM_REQUEST_CODE+=1;
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(System.currentTimeMillis());
    calendar.set(Calendar.HOUR_OF_DAY, hour);
    calendar.set(Calendar.MINUTE, min);
    AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    manager.setExact(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pendingIntent);
    Toast.makeText(this, "Alarm set for "+editTime.getText().toString(), Toast.LENGTH_LONG).show();
}
    public void PullUpTimePicker(View view) {
        calendar = Calendar.getInstance();
        currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        currentMin = calendar.get(Calendar.MINUTE);
        timePickerDialog = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                editTime.setText(String.format("%02d:%02d", hourOfDay, minutes));
            }
        }, currentHour, currentMin,false);
        timePickerDialog.show();
    }
}
