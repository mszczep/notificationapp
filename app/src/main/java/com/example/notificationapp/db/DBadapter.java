package com.example.notificationapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.notificationapp.model.recyclerViewModel;

public class DBadapter {

    private static final String DEBUG_TAG = "SqLiteListManager";
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "database.db";
    private static final String DB_LIST_TABLE = "list";
    public static final String KEY_ID = "_id";
    public static final String ID_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final int ID_COLUMN = 0;
    public static final String KEY_DESCRIPTION = "description";
    public static final String DESCRIPTION_OPTIONS = "TEXT NOT NULL";
    public static final int DESCRIPTION_COLUMN = 1;
    public static final String KEY_TIME = "time";
    public static final String TIME_OPTIONS = "TEXT NOT NULL";
    public static final int TIME_COLUMN=2;
    private static final String DB_CREATE_LIST_TABLE =
            "CREATE TABLE " + DB_LIST_TABLE + "( " +
                    KEY_ID + " " + ID_OPTIONS + ", " +
                    KEY_DESCRIPTION + " " + DESCRIPTION_OPTIONS + ", " +
                    KEY_TIME + " " + TIME_OPTIONS +
                    ");";
    private static final String DROP_LIST_TABLE =
            "DROP TABLE IF EXISTS " + DB_LIST_TABLE;

    private SQLiteDatabase db;
    private Context context;
    private DatabaseHelper dbHelper;

    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
        {
            super(context,name,factory,version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_LIST_TABLE);
            Log.d(DEBUG_TAG,"Database creating...");
            Log.d(DEBUG_TAG,"Table" +DB_LIST_TABLE+"ver."+DB_VERSION+"created");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            db.execSQL(DROP_LIST_TABLE);
            Log.d(DEBUG_TAG, "Database updating...");
            Log.d(DEBUG_TAG, "Table" + DB_LIST_TABLE + "updated from ver."+oldVersion+"to ver."+newVersion);
            Log.d(DEBUG_TAG,"All data is lost.");
            onCreate(db);
        }
    }

    public DBadapter (Context context) {this.context= context;}
    public DBadapter open(){
        dbHelper =new DatabaseHelper(context,DB_NAME,null,DB_VERSION);
        try{
            db = dbHelper.getWritableDatabase();
        } catch(SQLException e ){
            db = dbHelper.getReadableDatabase();
        }
        return this;
    }

    public void close(){dbHelper.close();}

    public long insertContent(String description, String hour){
        ContentValues newContentValues = new ContentValues();
        newContentValues.put(KEY_DESCRIPTION,description);
        newContentValues.put(KEY_TIME,hour);
        return db.insert(DB_LIST_TABLE,null,newContentValues);
    }
    public boolean updateList(recyclerViewModel content){
        long id = content.getId();
        String description = content.getNoteText();
        String hour = content.getHour();
        return updateList(id,description,hour);
    }

    public boolean updateList(long id, String description, String hour) {
        String where = KEY_ID+"="+id;
        ContentValues updateContentValues = new ContentValues();
        updateContentValues.put(KEY_DESCRIPTION, description);
        updateContentValues.put(KEY_TIME, hour);
        return db.update(DB_LIST_TABLE,updateContentValues,where,null)>0;
    }

    public boolean deleteContent(long id){
        String where = KEY_ID+"="+id;
        return db.delete(DB_LIST_TABLE,where,null)>0;
    }

    public void deleteAllContent(){
        db.delete(DB_LIST_TABLE,null,null);
    }

    public Cursor getAllContent(){
        String[] columns = {KEY_ID,KEY_DESCRIPTION,KEY_TIME};
        return db.query(DB_LIST_TABLE,columns,null,null,null,null,null);
    }

    public recyclerViewModel getContent (long id){
        String[] columns ={KEY_ID,KEY_DESCRIPTION,KEY_TIME};
        String where = KEY_ID+"="+id;
        Cursor cursor =db.query(DB_LIST_TABLE,columns,where,null,null,null,null);
        recyclerViewModel content =null;
        if(cursor !=null && cursor.moveToFirst()){
            String description = cursor.getString(DESCRIPTION_COLUMN);
            String time = cursor.getString(TIME_COLUMN);
            content = new recyclerViewModel(id, description,time);
        }
        return content;
    }

}
